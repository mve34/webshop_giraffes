-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 29 jun 2018 om 15:40
-- Serverversie: 10.1.19-MariaDB
-- PHP-versie: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webshop-complete`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(11) UNSIGNED NOT NULL,
  `amount` double(9,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `orders`
--

INSERT INTO `orders` (`id`, `amount`, `user_id`, `created_at`, `updated_at`) VALUES
(10, 13294.00, 47, '2018-06-29 12:57:23', '2018-06-29 12:57:23'),
(11, 13294.00, 48, '2018-06-29 13:03:29', '2018-06-29 13:03:29'),
(12, 13294.00, 49, '2018-06-29 13:08:41', '2018-06-29 13:08:41');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `orders_products`
--

CREATE TABLE `orders_products` (
  `id` int(11) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` double(9,2) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `orders_products`
--

INSERT INTO `orders_products` (`id`, `order_id`, `product_id`, `price`, `quantity`, `created_at`, `updated_at`) VALUES
(17, 47, 4, 2199.00, 2, '2018-06-29 12:57:23', '2018-06-29 12:57:23'),
(18, 47, 2, 2299.00, 3, '2018-06-29 12:57:23', '2018-06-29 12:57:23'),
(19, 47, 1, 1999.00, 1, '2018-06-29 12:57:23', '2018-06-29 12:57:23'),
(20, 48, 4, 2199.00, 2, '2018-06-29 13:03:29', '2018-06-29 13:03:29'),
(21, 48, 2, 2299.00, 3, '2018-06-29 13:03:29', '2018-06-29 13:03:29'),
(22, 48, 1, 1999.00, 1, '2018-06-29 13:03:29', '2018-06-29 13:03:29'),
(23, 49, 4, 2199.00, 2, '2018-06-29 13:08:41', '2018-06-29 13:08:41'),
(24, 49, 2, 2299.00, 3, '2018-06-29 13:08:41', '2018-06-29 13:08:41'),
(25, 49, 1, 1999.00, 1, '2018-06-29 13:08:41', '2018-06-29 13:08:41');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `products`
--

CREATE TABLE `products` (
  `id` int(11) UNSIGNED NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `price` double(9,2) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `products`
--

INSERT INTO `products` (`id`, `slug`, `title`, `description`, `price`, `image`, `seo_title`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, 'northern-giraffe', 'Northern Giraffe', 'This is A Northern Giraffe', 1999.00, 'northern_giraffe.jpg', 'northern giraffe', 'This is A Northern Giraffe', '2018-05-29 11:47:00', NULL),
(2, 'somali-giraffe', 'Somali Giraffe', 'This is a Somali Giraffe', 2299.00, 'somali_giraffe.jpg', 'Somali Giraffe', 'This is a Somali Giraffe', '2018-05-29 11:48:00', NULL),
(3, 'rothschild-giraffe', 'Rothschild Giraffe', 'This is a Rothschild Giraffe', 2999.00, 'rothschild_giraffe.jpg', 'Rothschild Giraffe', 'This is a Rothschild Giraffe', '2018-05-29 11:49:00', NULL),
(4, 'chad-giraffe', 'Chad giraffe', 'This is a Chad Giraffe', 2199.00, 'chad_giraffe.jpg', 'Chad Giraffe', 'This is a Chad Giraffe', '2018-05-29 11:50:00', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL DEFAULT '',
  `suffix_name` varchar(255) DEFAULT '',
  `last_name` varchar(255) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT 'NL',
  `city` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `street_number` varchar(255) NOT NULL,
  `street_suffix` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `remember_token`, `first_name`, `suffix_name`, `last_name`, `country`, `city`, `street`, `street_number`, `street_suffix`, `zipcode`, `active`, `created_at`, `updated_at`) VALUES
(47, 'mve34@hotmail.nl', '$2y$10$5TOer1hx5UNic1zSJuhVkOpzUZpOmepgYwVy/qz94bqF/j7AaTDQy', '', 'Mikal', 'van', 'Eijk', 'Nederland', 'Diemen', 'Gravenland', '34', '', '1111 SM ', 0, '2018-06-29 12:57:23', '2018-06-29 12:57:23'),
(48, 'mve345@hotmail.nl', '$2y$10$FwXVUkMkhCxEuttLCnBmkOgwh6yTjPs0Hp/jjZ.34d18SGYts3BAa', '', 'Mikal', 'van', 'Eijk', 'Nederland', 'Diemen', 'Gravenland', '34', '', '1111 SM ', 0, '2018-06-29 13:03:29', '2018-06-29 13:03:29'),
(49, 'mve314@hotmail.nl', '$2y$10$I9NUe7K2Y3ok0jxlAYx2iuqNjXdNRN/N2AupwEfyZvSBgkM/c9Ldu', '', 'Mikal', 'van', 'Eijk', 'Nederland', 'Diemen', 'Gravenland', '34', '', '1111 SM ', 0, '2018-06-29 13:08:41', '2018-06-29 13:08:41');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `orders_products`
--
ALTER TABLE `orders_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT voor een tabel `orders_products`
--
ALTER TABLE `orders_products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT voor een tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
