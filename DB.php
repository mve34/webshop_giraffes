<?php

Class DB {

    private $connection;
    private $query;
    private $host = 'localhost';
    private $database = 'webshop-jopie';
    private $username = 'root';
    private $password = 'root';
    private $port = 3306;
    private $parameters = [];


    public function __construct()
    {
        $this->connect();
    }


    public function connect()
    {
        try {
            $this->connection = new PDO('mysql:host='.$this->host.';dbname='.$this->database, $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e) {
            dd($e->getMessage());
        }
    }


    public function query($query, $parameters = [])
    {

        $this->query = $this->connection->prepare($query);
        $this->parameters = $parameters;

        return $this;

    }


    public function select()
    {
        $this->query->execute($this->parameters);

        $this->query->setFetchMode(PDO::FETCH_ASSOC);

        return $this->query->fetchAll();
    }

    // mvg Ammaar
    public function insert()
    {
        try {
            $this->query->execute($this->parameters);
            return $this->connection->lastInsertId();
        }
        catch(PDOException $e) {
            dd($e->getMessage());
        }
    }


    public function delete()
    {

    }


    public function update()
    {

    }

    // side note: Issam en Burak weet hier alles van
    public function next()
    {

    }

}

// load all the base functions
function dd($text)
{
    if(is_array($text) || is_object($text)) {
        var_dump($text);
        die();
    }
    else {
        die($text);
    }
}


$db = new DB();

// $data = $db->query('SELECT * FROM personeel where id = :id', ['id' => $_GET['id']])->select();

// dd($data);

$id = $db->query('INSERT INTO personeel (personeel_nummer, personeel_naam, afdeling_id)
    VALUES
    (:personeel_nummer, :personeel_naam, :afdeling_id)
    ', [
    'personeel_nummer' => '20',
    'personeel_naam' => 'Wouter',
    'afdeling_id' => '1',
])->insert();



dd($id);
?>

