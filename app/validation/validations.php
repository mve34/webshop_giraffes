<?php
//een lege array voor alle errors
$errors = [];

//alle type validaties
$validations = [
    'required',
    'number',
    'email',
    'name',
    'min',
    'max',
    'confirmed',
    'postcode',
];

foreach ($variables as $key => $checks) {


    foreach ($checks as $check) {


        $checkExploded = explode(':', $check);

        if (count($checkExploded) > 1) {


            // wel een :
            $checkFunction = 'is' . ucfirst($checkExploded[0]);
            if ($error = $checkFunction($_POST[$key], $checkExploded[1], $key, $checks)) {

                if (array_key_exists($key, $errors)) {
                    array_push($errors[$key], $error);
                } else {
                    $errors[$key] = [$error];
                }
            }

        } else {
            // geen :
            $checkFunction = 'is' . ucfirst($check);

            if ($error = $checkFunction($_POST[$key], $key, $checks)) {


                if (array_key_exists($key, $errors)) {
                    array_push($errors[$key], $error);
                } else {
                    $errors[$key] = [$error];
                }
            }
        }
    }
}

//checken of het input veld leeg is
function isRequired($value, $key, $checks)
{
    //als hij leeg is:
    if (!$value) {
        return 'U heeft geen waarde ingevuld';
    }
}

//checken of het alleen maar getallen zijn
function isNumber($value, $key, $checks)
{
    //als het niet alleen maar getallen zijn:
    if ($value && !is_numeric($value)) {
        return 'U heeft geen numerieke waarde ingevuld';
    }
}

//checken of het een postcode is
function isPostcode($value, $key, $checks)
{
    //als het geen geldide postcode is
    if ($value && !preg_match('/^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i', $value)) {
        return 'U heeft geen correcte postcode ingevuld';
    }
}

//checken of het een email is
function isEmail($value, $key, $checks)
{
    //als het geen geldige email is
    if ($value && !preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $value)) {
        return 'U heeft geen geldig e-mail adres ingevuld';
    }
}

//het mogen alle letters zijn en streepjes en zo
function isName($value, $key, $checks)
{
    //als het iets anders is dan dat mag
    if ($value && !preg_match('/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$/u', $value)) {
        return 'U heeft geen geldige waarde ingevuld';
    }
}

//checken of 2 waardes hetzelfde zijn
function isConfirmed($value, $key, $checks)
{
    //als ze niet het zelfde zijn
    if ($value && $_POST[$key] != $_POST[$key . '_confirmed']) {
        return 'Het confirmatie veld heeft niet dezelfde waarde';
    }
}

//checken of het minder dan het minimum is
function isMin($value, $amount, $key, $checks)
{
    //als het minder dan het minimum is
    if ($value && strlen($value) < $amount) {
        return 'U heeft niet voldoende tekens ingevoerd. Minimaal ' . $amount;
    }
}

//checken of het meer dan het maximum is
function isMax($value, $amount, $key, $checks)
{
    //als het meer dan het maximum is
    if ($value && strlen($value) > $amount) {
        return 'U heeft teveel tekens ingevoerd. Maximaal ' . $amount;
    }
}