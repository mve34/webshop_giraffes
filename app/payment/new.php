<?php

try {
    //voer de functie db() uit in de variable $connection
    $connection = db();
    $connection->beginTransaction();

    //zet de ingevoerde waarde van het email veld in de variable $email
    $email = $_POST['email'];
    //maak een query aan waarin je email ophaalt waar de email hetzelfde het vraagteken
    $query = $connection->prepare("SELECT `email` FROM `users` WHERE `email` = ?");
    //hier bind je het vraagteken aan de variable $email
    $query->bindValue(1, $email);
    //hier voer je de query uit
    $query->execute();

    //als de email al in de database staat geeft hij een alert en doet hij verder niks
    if ($query->rowCount() > 0) { # If rows are found for query
        echo "<script>alert('Deze e-mail word al gebruikt')</script>";
    } else {
        //als de email nog niet bestaat:

        // user aanmaken


        $query = 'INSERT INTO `users` (first_name, suffix_name, last_name, country, city, street, street_number, street_suffix, zipcode, email, password, created_at, updated_at)
            VALUES
            (:first_name, :suffix_name, :last_name, :country, :city, :street, :street_number, :street_suffix, :zipcode, :email, :password, :created_at, :updated_at)';

        //alle waarden op de juiste manier in de database zetten
        $data = [
            'first_name' => standardizeName($_POST['first_name']),
            'suffix_name' => trim($_POST['suffix_name']),
            'last_name' => standardizeName($_POST['last_name']),
            'country' => $_POST['country'],
            'city' => standardizeName($_POST['city']),
            'street' => standardizeName($_POST['street']),
            'street_number' => $_POST['street_number'],
            'street_suffix' => trim($_POST['street_suffix']),
            'zipcode' => standardizePostcode($_POST['zipcode']),
            'email' => $_POST['email'],
            'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
            'created_at' => date('y-m-d H-i-s'),
            'updated_at' => date('y-m-d H-i-s'),
        ];


        //query voorbereiden
        $products = $connection->prepare($query);
        //query uitvoeren
        $products->execute($data);




        // order aanmaken
        $query = 'INSERT INTO `orders`
        (amount, user_id, created_at, updated_at)
        VALUES
        (:amount, :user_id, :created_at, :updated_at)';
        $userId = $connection->lastInsertId();
        $products = $connection->prepare($query); // query voorbereiden
        $products->execute([
            'amount' => $_SESSION['cart']['total'],
            'user_id' => $userId,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),

        ]);



        // product_order
        $query = 'INSERT INTO orders_products (order_id, product_id, price, quantity, created_at, updated_at)
       VALUES (:order_id, :product_id, :price, :quantity, :created_at, :updated_at)';

        $orderProduct = $connection->prepare($query); // query voorbereiden
        $orderId = $connection->lastInsertId();
        foreach ($_SESSION['cart']['products'] as $id => $product) {

            $orderProduct->execute([

                'order_id' => $orderId,
                'product_id' => $product['id'],
                'price' => $product['price'],
                'quantity' => $product['quantity'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

        }

        $connection->commit();
        //redirecten naar de succes.php pagina
        unset($_SESSION['cart']);
        header("Location: " . asset('succes.php'));


    }
} //als hij op zn bek gaat gaat hij terug zodat het niet alle opfokt in de database
catch (Exception $e) {
    $connection->rollBack();
}
//hier zorgt hij dat de postcode altijd met hoofdletters is en een spatie tussen de letters en getallen
function standardizePostcode($postcode)
{
    return strtoupper(chunk_split($postcode, 4, ' '));
}

//hier doet hij alle woorden met een hoofdletter en haalt hij de spaties aan het begin en het einde weg
function standardizeName($string)
{
    return ucwords(trim($string));
}