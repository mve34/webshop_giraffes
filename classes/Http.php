<?php

Class Http //maak nieuwe class aan
{

    public static $webroot = ''; //maak nieuwe publiekelijke statische variable webroot aan die leeg is


    public static function boot() //maak nieuwe publiekelijke functie boot aan
    {
        // zoeken naar localhost
        // zoeken naar /public/
        // webroot van maken


        if ($_SERVER['HTTP_HOST'] == 'localhost' && strpos($_SERVER['REQUEST_URI'], '/public/')) { //if statement die checkt of de http host gelijk is aan localhost en hij checkt of er in de URI /public/ staat

            $urlParts = explode('/public/', $_SERVER['REQUEST_URI']); //hij splitst de URI waar /public/ staat

            self::$webroot = self::httpOrHttps() . $_SERVER['HTTP_HOST'] . $urlParts[0] . '/public/'; //
        } else {
            self::$webroot = $_SERVER['HTTP_HOST'];
        }

    }


    public static function webroot()
    {
        return self::$webroot;
    }


    private static function httpOrHttps()
    {
        if (isset($_SERVER['HTTPS'])) {
            return 'https://';
        }
        return 'http://';
    }

}