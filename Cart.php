<?php

$cart = new Cart;

class Cart
{
    /**
     * Shipping costs per kilo
     * @var float
     */
    private $shippingCosts = 1.67; // per kilo

    /**
     * The shopping cart content
     * @var array
     */
    private $cart = [
        'items' => [],
        'price' => 0,
        'shipping-costs' => 0,
        'weight' => 0,
        'amount' => 0,
    ];

    /**
     * The products we sell
     * @var array
     */
    private $products = [
        'K1' => [
            'title' => 'Kaarsen',
            'price' => 10.99,
            'weight' => 0.9,
            'amount' => 0,
        ],
        'K2' => [
            'title' => 'Onderzetters',
            'price' => 1.55,
            'weight' => 0.1,
            'amount' => 0,
        ],
        'K3' => [
            'title' => 'Rietjes',
            'price' => 0.99,
            'weight' => 0.03,
            'amount' => 0,
        ],
        'K4' => [
            'title' => 'Glazen',
            'price' => 12.35,
            'weight' => 1.5,
            'amount' => 0,
        ],

    ];

    /**
     * adds an item to the cart
     */
    public function addToCart($articleNumber){
        if (isset($this->cart['items'][$articleNumber])){
            $this->cart['items'][$articleNumber]['amount']++;
            $this->cart['amount']++;
        }
        else {
            $this->cart['items'][$articleNumber] = $this->products[$articleNumber];
            $this->cart['items'][$articleNumber]['amount'] = 1;
            $this->cart['amount']++;
        }

        $this->calculate();

        return $this;
    }

    public function removeFromCart($articleNumber){
        if (isset($this->cart['items'][$articleNumber])){
            if ($this->cart['items'][$articleNumber]['amount'] > 1){
                $this->cart['items'][$articleNumber]['amount']--;
                $this->cart['amount']--;
            }else{
                unset($this->cart['items'][$articleNumber]);
            }
        }

        $this->calculate();

        return $this;
    }

    public function get(){
        return $this->cart;
        return $this;
    }

    public function destroy(){
        $this->cart = [
            'items' => [],
            'price' => 0,
            'shipping-costs' => 0,
            'weight' => 0,
            'amount' => 0,
        ];
        return $this;
    }

    private function calculate(){
        $totalPrice = 0;
        $totalWeight = 0;
        $totalShippingCosts = 0;

        foreach ($this->cart['items'] as $code => $item){
            $totalPrice += $this->cart['items'][$code]['price'] * $this->cart['items'][$code]['amount'];
            $totalWeight += $this->cart['items'][$code]['weight'] * $this->cart['items'][$code]['amount'];
        }

        $this->cart['price'] = $totalPrice;
        $this->cart['weight'] = $totalWeight;
        $this->cart['shipping-costs'] = $this->shippingCosts * $totalWeight;
        return $this;
    }

}

$cart = new Cart();

$cart
    ->addToCart('K1')
    ->addToCart('K2')
    ->addToCart('K2')
    ->removeFromCart('K2');

print_r($cart->get());
