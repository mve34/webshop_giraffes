Welkom en bedankt dat u voor mijn webshop hebt gekozen.

Na het downloaden van het .zip bestand moet u die uitpakken in een online omgeving of op uw localhost.
Hierna ziet u in de map database een .sql bestand die u moet importeren in uw database omgeving.


nadat u dat gedaan heeft opent u het boot.php bestand en wijzigt u daar:
- op line 20, de variable $host moet de naam van uw host worden.
- op line 21, de variable $batabase moet de naam van uw database worden.
- op line 22, de variable $username moet de username van uw database worden.
- op line 23, de variable $password moet het wachtwoord van uw database worden.

Ik hoop dat dit allemaal duidelijk was, veel plezier met mijn webshop.

Mikal van Eijk