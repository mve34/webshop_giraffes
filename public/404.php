<?php
//hier require je de boot.php
require '../boot.php';

?>

<!DOCTYPE html>
<html>
<title>Giraffes for sale</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="stylesheet" href="<?php echo asset('css/w3.css'); ?>"/>
<link rel="stylesheet" href="<?php echo asset('css/roboto.css'); ?>"/>
<link rel="stylesheet" href="<?php echo asset('css/monsterrat.css'); ?>"/>
<link rel="stylesheet" href="<?php echo asset('css/font-awesome.css') ?>"/>
<link rel="stylesheet" href="<?php echo asset('css/main.css') ?>"/>
<style>
    .w3-sidebar a {
        font-family: "Roboto", sans-serif
    }

    body, h1, h2, h3, h4, h5, h6, .w3-wide {
        font-family: "Montserrat", sans-serif;
    }
</style>
<body class="w3-content" style="max-width:1200px">

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-bar-block w3-white w3-collapse w3-top" style="z-index:3;width:250px" id="mySidebar">
    <div class="w3-container w3-display-container w3-padding-16" href="index.php">
        <h3 class="w3-wide" href="<?php echo asset('index.php') ?>"><a
                    href="<?php echo asset('index.php') ?>"><b>LOGO</b></a></h3>
    </div>
    <div class="w3-padding-64 w3-large w3-text-grey" style="font-weight:bold">
        <a href="<?php echo asset('producten.php') ?>" class="w3-button w3-block w3-white w3-left-align" id="myBtn">
            Giraffes
        </a>
    </div>
    <a href="#footer" class="w3-bar-item w3-button w3-padding">Contact</a>
</nav>

<!-- Top menu on small screens -->
<header class="w3-bar w3-top w3-hide-large w3-black w3-xlarge">
    <div class="w3-bar-item w3-padding-24 w3-wide"><a href="<?php echo asset('index.php') ?>">LOGO</a></div>
    <a class="w3-bar-item w3-button w3-padding-24 w3-right"><i class="fa fa-bars"></i></a>
</header>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu"
     id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:250px">

    <!-- Push down content on small screens -->
    <div class="w3-hide-large" style="margin-top:83px"></div>

    <!-- Top header -->
    <header class="w3-container w3-xlarge">
        <p class="w3-left">Giraffes</p>
        <p class="w3-right">
            <button id="openModalButton">Shopping cart</button>
            <a href="<?php echo asset('login.php')?>">
                <button id="login_btn">login</button>
            </a>
        </p>
    </header>

    <h1>404 - page not found</h1><br/>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

</body>
</html>


