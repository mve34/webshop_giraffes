<?php
//functionele gedeelte

//hier require je de boot.php voor de database connectie
require '../boot.php';

//als de request method post is gaat hij dit doen
if ($_SERVER['REQUEST_METHOD'] === 'POST') {


    //dit is van alle input velden wat er allemaal in mag
    $variables = [
        'first_name' => ['required', 'min:2', 'max:50', 'name'],
        'suffix_name' => ['min:1', 'max:15', 'name'],
        'last_name' => ['required', 'name', 'min:2', 'max:50'],
        'country' => ['min:2', 'max:15', 'name'],
        'city' => ['required', 'min:2', 'max:55', 'name'],
        'street' => ['required', 'min:2', 'max:85', 'name'],
        'street_number' => ['required', 'min:1', 'max:5'],
        'street_suffix' => ['min:1', 'max:25'],
        'zipcode' => ['required', 'postcode', 'min:6', 'max:7'],
        'email' => ['required', 'email', 'min:7', 'max:155'],
        'password' => ['required', 'min:8', 'max:100', 'confirmed'],
    ];

    //hier require je de validations.php om te controleren of alles goed is in gevuld
    require '../app/validation/validations.php';

    //als alles goed is ingevuld require je new.php om de ingevoerde waardes in de database te zetten
    if (count($errors) === 0) {
        require '../app/payment/new.php';
    }
} else {
    // doe query
    // zet output van query in $_POST
}

function value($key)
{
    return @$_POST[$key];
}

//tempalting gedeelte
?>

<!DOCTYPE html>
<html>
<title>Giraffes for sale</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="stylesheet" href="<?php echo asset('css/w3.css'); ?>"/>
<link rel="stylesheet" href="<?php echo asset('css/roboto.css'); ?>"/>
<link rel="stylesheet" href="<?php echo asset('css/monsterrat.css'); ?>"/>
<link rel="stylesheet" href="<?php echo asset('css/font-awesome.css') ?>"/>
<link rel="stylesheet" href="<?php echo asset('css/main.css') ?>"/>

<body class="w3-content" style="max-width:1200px">

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-bar-block w3-white w3-collapse w3-top" style="z-index:3;width:250px" id="mySidebar">
    <div class="w3-container w3-display-container w3-padding-16" href="<?php echo asset('index.php') ?>">
        <h3 class="w3-wide" href="<?php echo asset('index.php') ?>"><a
                    href="<?php echo asset('index.php') ?>"><b>LOGO</b></a></h3>
    </div>
    <div class="w3-padding-64 w3-large w3-text-grey" style="font-weight:bold">
        <a href="<?php echo asset('producten.php') ?>" class="w3-button w3-block w3-white w3-left-align" id="myBtn">
            Giraffes
        </a>
    </div>
    <a href="#footer" class="w3-bar-item w3-button w3-padding">Contact</a>
</nav>

<!-- Top menu on small screens -->
<header class="w3-bar w3-top w3-hide-large w3-black w3-xlarge">
    <div class="w3-bar-item w3-padding-24 w3-wide"><a href="<?php echo asset('index.php') ?>">LOGO</a></div>
    <a class="w3-bar-item w3-button w3-padding-24 w3-right"><i class="fa fa-bars"></i></a>
</header>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu"
     id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:250px">

    <!-- Push down content on small screens -->
    <div class="w3-hide-large" style="margin-top:83px"></div>

    <!-- Top header -->
    <header class="w3-container w3-xlarge">
        <p class="w3-left">Giraffes</p>
    </header>


    <!-- het formulier -->
    <h2>Welcome to the payment!</h2>
    <p>Fill in your personal info here:</p>
    <form method="post">

        <label>First name:
            <input type="text" name="first_name" id="first_name"
                   value="<?php echo value('first_name'); ?>"/><br/></label>
        <?php echo (@$errors['first_name']) ? '<p class="text-danger">' . $errors['first_name'][0] . '</p>' : ''; ?>
        <label>suffix name:
            <input type="text" name="suffix_name" id="suffix_name"
                   value="<?php echo value('suffix_name'); ?>"/><br/></label>
        <?php echo (@$errors['suffix_name']) ? '<p class="text-danger">' . $errors['suffix_name'][0] . '</p>' : ''; ?>
        <label>last name:
            <input type="text" name="last_name" id="last_name" value="<?php echo value('last_name'); ?>"/><br/></label>
        <?php echo (@$errors['last_name']) ? '<p class="text-danger">' . $errors['last_name'][0] . '</p>' : ''; ?>
        <label>country:
            <input type="text" name="country" id="country" value="<?php echo value('country'); ?>"/><br/></label>
        <?php echo (@$errors['country']) ? '<p class="text-danger">' . $errors['country'][0] . '</p>' : ''; ?>
        <label>city:
            <input type="text" name="city" id="city" value="<?php echo value('city'); ?>"/><br/></label>
        <?php echo (@$errors['city']) ? '<p class="text-danger">' . $errors['city'][0] . '</p>' : ''; ?>
        <label>street:
            <input type="text" name="street" id="street" value="<?php echo value('street'); ?>"/><br/></label>
        <?php echo (@$errors['street']) ? '<p class="text-danger">' . $errors['street'][0] . '</p>' : ''; ?>
        <label>street number:
            <input type="text" name="street_number" id="street_number"
                   value="<?php echo value('street_number'); ?>"/><br/></label>
        <?php echo (@$errors['street_number']) ? '<p class="text-danger">' . $errors['street_number'][0] . '</p>' : ''; ?>
        <label>street suffix:
            <input type="text" name="street_suffix" id="street_suffix"
                   value="<?php echo value('street_suffix'); ?>"/><br/></label>
        <?php echo (@$errors['street_suffix']) ? '<p class="text-danger">' . $errors['street_suffix'][0] . '</p>' : ''; ?>
        <label>zipcode:
            <input type="text" name="zipcode" id="zipcode" value="<?php echo value('zipcode'); ?>"/><br/></label>
        <?php echo (@$errors['zipcode']) ? '<p class="text-danger">' . $errors['zipcode'][0] . '</p>' : ''; ?>
        <label>email:
            <input type="email" name="email" id="email" value="<?php echo value('email'); ?>"/><br/></label>
        <?php echo (@$errors['email']) ? '<p class="text-danger">' . $errors['email'][0] . '</p>' : ''; ?>
        <label>password:
            <input type="password" name="password" id="password" /><br/></label>
        <label>confirm password:
            <input type="password" name="password_confirmed" id="password_confirmed"/><br/></label>
        <?php echo (@$errors['password']) ? '<p class="text-danger">' . $errors['password'][0] . '</p>' : ''; ?>
        <button type="submit">submit</button>
    </form>

    <!-- de shopping cart -->
    <span class="close">&times;</span>
    <!--    hier begint de shopping cart bucket-->
    <aside class="bucket" id="bucket">
        <?php include "partials/bucket.php" ?>
    </aside>


    <!-- Footer -->
    <footer class="w3-padding-64 w3-light-grey w3-small w3-center" id="footer">
        <div class="w3-row-padding">
            <div class="w3-col s4">
                <h4>Contact</h4>
                <p>Questions? Go ahead.</p>
                <form action="<?php echo asset('/action_page.php'); ?>" target="_blank">
                    <p><input class="w3-input w3-border" type="text" placeholder="Name" name="Name" required></p>
                    <p><input class="w3-input w3-border" type="text" placeholder="Email" name="Email" required></p>
                    <p><input class="w3-input w3-border" type="text" placeholder="Subject" name="Subject" required></p>
                    <p><input class="w3-input w3-border" type="text" placeholder="Message" name="Message" required></p>
                    <button type="submit" class="w3-button w3-block w3-black">Send</button>
                </form>
            </div>

            <div class="w3-col s4">
                <h4>About</h4>
                <p><a href="#">About us</a></p>
                <p><a href="#">We're hiring</a></p>
                <p><a href="#">Support</a></p>
                <p><a href="#">Find store</a></p>
                <p><a href="#">Shipment</a></p>
                <p><a href="#">Payment</a></p>
                <p><a href="#">Gift card</a></p>
                <p><a href="#">Return</a></p>
                <p><a href="#">Help</a></p>
            </div>

            <div class="w3-col s4 w3-justify">
                <h4>Store</h4>
                <p><i class="fa fa-fw fa-map-marker"></i> Company Name</p>
                <p><i class="fa fa-fw fa-phone"></i> 0044123123</p>
                <p><i class="fa fa-fw fa-envelope"></i> ex@mail.com</p>
                <h4>We accept</h4>
                <p><i class="fa fa-fw fa-cc-amex"></i> Amex</p>
                <p><i class="fa fa-fw fa-credit-card"></i> Credit Card</p>
                <br>
                <i class="fa fa-facebook-official w3-hover-opacity w3-large"></i>
                <i class="fa fa-instagram w3-hover-opacity w3-large"></i>
                <i class="fa fa-snapchat w3-hover-opacity w3-large"></i>
                <i class="fa fa-pinterest-p w3-hover-opacity w3-large"></i>
                <i class="fa fa-twitter w3-hover-opacity w3-large"></i>
                <i class="fa fa-linkedin w3-hover-opacity w3-large"></i>
            </div>
        </div>
    </footer>


    <!-- End page content -->
</div>

<!-- Newsletter Modal -->
<div id="newsletter" class="w3-modal">
    <div class="w3-modal-content w3-animate-zoom" style="padding:32px">
        <div class="w3-container w3-white w3-center">
            <i onclick="document.getElementById('newsletter').style.display='none'"
               class="fa fa-remove w3-right w3-button w3-transparent w3-xxlarge"></i>
            <h2 class="w3-wide">NEWSLETTER</h2>
            <p>Join our mailing list to receive updates on new arrivals and special offers.</p>
            <p><input class="w3-input w3-border" type="text" placeholder="Enter e-mail"></p>
            <button type="button" class="w3-button w3-padding-large w3-red w3-margin-bottom"
                    onclick="document.getElementById('newsletter').style.display='none'">Subscribe
            </button>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

<script src="javascript/bucket.js"></script>


</body>
</html>


