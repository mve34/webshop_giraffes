$(document).ready(function () {

    bucket();
});


function bucket() {
    $('.add-to-cart, .remove-from-cart').unbind('click').click(function (event) {
        event.preventDefault();

        // alert($(this).data('url'));

        jQuery.ajax($(this).data('url'), {
            method: 'post',
            cache: false,
            // dataType: 'json',
        })
            .done(function (data) {
                if (data) {
                    $('#bucket').html(data);
                    bucket();
                }
            })
            .fail(function () {
                alert("error");
                bucket();
            });
    });
}


// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("openModalButton");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}