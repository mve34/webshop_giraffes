<h2>Shoppingcart</h2>

<table class="table">
    <thead>
    <tr>
        <th>Titel</th>
        <th></th>
        <th>Aantal</th>
        <th>Prijs</th>
    </tr>
    </thead>
    <tbody>

    <?php //een foreach die alle producten die in de cart zitten in de variable $cartProduct zet
    foreach ($_SESSION['cart']['products'] as $cartProduct) { ?>
        <tr>
            <td><?php //hier halt hij de title op uit de session
                echo $cartProduct['title']; ?></td>
            <td>
                <button type="button" class="w3-left addItem btn btn-warning add-to-cart"
                        data-url="<?php //hier haalt hij het id van het product op om er een bij te zetten als je op de button drukt
                        echo asset('cart/add.php?id=' . $cartProduct['id']); ?>">
                    +
                </button>
                <button type="button" class="w3-left removeItem btn btn-warning add-to-cart"
                        data-url="<?php //hier haalt hij het id van het product op om er een af te halen als je op de button drukt
                        echo asset('cart/remove.php?id=' . $cartProduct['id']); ?>">
                    -
                </button>
            </td>
            <td><?php //hier laat hij de hoeveelheid van het product zien
                echo $cartProduct['quantity']; ?></td>
            <td><?php //hier laat gij de prijs van het product zien
                echo $cartProduct['price']; ?></td>
        </tr>

    <?php } ?>
    </tbody>
</table>


<h3>Totaal:<br/>
    <?php //hier laat hij de totaal prijs zien
    echo '€' . $_SESSION['cart']['total']; ?></h3>
<h5>click <a href="<?php echo asset("pay.php") ?>">hier</a> om te betalen</h5>

